---
title: IBISBA
summary: Striving to facilitate end-to-end bioprocess development
tags:
  - IBISBA
  - Infrastructure
  - Biotechnology
date: "2018-09-11T00:00:00Z"

# Optional external URL for project (replaces project detail page).
# external_link: https://www.ibisba.eu

# image:
#   caption: Photo by Toa Heftiba on Unsplash
# focal_point: Smart
---
**IBISBA 1.0 – striving to facilitate end-to-end bioprocess development**

**Vision**

Accounting for the key status of industrial biotechnology, this area merits special focus and more common effort. Biocatalysis provides unique capability to convert complex biobased resources into a whole range of valuable products, but the full potential of industrial biotechnology as a manufacturing technology has not yet been reached. To achieve this, the R&I phases that convert concepts into pre-industrial processes must be accelerated and the resulting bioprocesses need to be increasingly robust, reliable and resilient.

Europe possesses a lot of research infrastructures that can be used to accelerate the development of innovative bioprocesses. Currently, these are mostly disconnected and thus unable to host the development of efficient R&I project pipelines. However, networking of individual infrastructure facilities is a viable way to overcome this problem, reduce duplication of research and cut investment, create synergy and stimulate cross-fertilisation among experts from different scientific theme areas.

**Mission**

IBISBA 1.0 is a H2020-funded project that aims at building a European distributed network of research facilities to provide innovation services and accelerate translation of bioscience research into industrial applications.

IBISBA 1.0 provides access to a specialised research facilities for all industrial biotechnology professionals, including academic researchers, SMEs and large companies.

The IBISBA 1.0 network also offers subsidised access through its Transnational Access (TNA) programme and will develop a variety of activities, such as training, that will benefit the wider scientific community.

Focusing on e-services, the IBISBA 1.0 project will develop a dedicated knowledge hub that will provide users with FAIR access to a wide variety of project assets, such as standard operating protocols (SOPs), workflows publically-accessible, project deliverables and datasets.

Looking to the future, the overarching aim of IBISBA 1.0 project is to create a European distributed research infrastructure that will provide a range of research support services to the Industrial Biotechnology community in Europe and worldwide.
