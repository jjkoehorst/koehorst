---
title: UNLOCK
summary: Striving to facilitate end-to-end bioprocess development
tags:
  - UNLOCK
  - Infrastructure
  - Biotechnology
date: "2018-09-11T00:00:00Z"

# Optional external URL for project (replaces project detail page).
# external_link: https://www.ibisba.eu

# image:
#   caption: Photo by Toa Heftiba on Unsplash
# focal_point: Smart
---
**UNLOCK – Unlocking the potential of microbial communities**

UNLOCK is a unique facility for research on mixed microbial cultures, the first of its kind worldwide. Wageningen University and Delft University of Technology have joined forces with UNLOCK to better exploit microorganisms present in nature, and to solve some of the major societal challenges facing food safety and production, human and animal health, environment and bioresource utilization and sustainable production of bio-based chemicals. Owing to the smooth access procedure, the experimental hardware available, and the expertise of involved researchers, UNLOCK enables research that to date is considered too complex and too expensive for one researcher (or one research group) to conduct. 
