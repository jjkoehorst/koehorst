---
# Display name
name: Dr. Jasper Koehorst

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Researcher On Semantics & Systems Biology

# Organizations/Affiliations
organizations:
- name: Wageningen University
  url: "https://www.wur.nl"

# Short bio (displayed in user profile at end of posts)
# bio: My research interests include Systems Biology, Semantic Web and the F.A.I.R principles.

interests:
- Semantic Web
- Ontology development
- Microbial genetics
- Big data analytics
- Making data FAIR

education:
  courses:
  - course: PhD in Systems Biology
    institution: Wageningen University
    year: 2019
  - course: Msc in Computational Biology
    institution: Wageningen University
    year: 2012
  - course: BSc in Molecular Life Sciences
    institution: Hogeschool van Arnhem & Nijmegen
    year: 2010

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:jasperkoehorst@gmail.com'  # For a direct email link, use "mailto:jasper.koehorst@wur.nl".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/jjkoehorst
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.nl/citations?user=KAfqpDAAAAAJ
- icon: gitlab
  icon_pack: fab
  link: http://gitlab.com/jjkoehorst
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
# user_groups:
# - Researchers
# - Visitors
---

I am a Computational Biologist & Ontology developer at the Department of Systems and Synthetic Biology at Wageningen University. This is where I work together on developing FAIR data systems intwined with biology. In addition I teach students to use computational methods in understanding biology and how the Semantic Web can help integrating data from an unlimited variety of resources. 



<!-- Nelson Bighetti is a professor of artificial intelligence at the Stanford AI Lab. His research interests include distributed robotics, mobile computing and programmable matter. He leads the Robotic Neurobiology group, which develops self-reconfiguring robots, systems of self-organizing robots, and mobile sensor networks. -->

<!-- I love to work and collaborate with other people on unraveling hidden conne... -->

<!--  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed neque elit, tristique placerat feugiat ac, facilisis vitae arcu. Proin eget egestas augue. Praesent ut sem nec arcu pellentesque aliquet. Duis dapibus diam vel metus tempus vulputate.  -->
