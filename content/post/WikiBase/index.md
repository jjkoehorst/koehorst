---
title: WikiBase
summary: Setting up a wikibase and all the things I encountered
tags:
  - WikiBase
  - Infrastructure
date: "2018-07-17T00:00:00Z"

# Optional external URL for project (replaces project detail page).
# external_link: https://www.ibisba.eu

# image:
#   caption: Photo by Toa Heftiba on Unsplash
# focal_point: Smart
---
**WikiBase – Creating your own WikiBase node**

This will be a continous growing document of all the fantastic, annoying, strange things I encountered to setup a wikibase using docker. It will be partly used for myself to keep track of all the things I encountered along the way and hopefully a helpfull guide in setting up your own wikibase.

### Starting a wikibase
The inital setup of a wikibase is extremely easy to do and only requires a .yml file containing all the properties required to setup a wikibase. The fundamental basis of your wikibase can be obtained from https://github.com/wmde/wikibase-docker. The `docker-compose.yml` provides a perfect basis for setting this up.

To start the wikibase:
- Create a folder (which will be the docker base name) and place the yaml file inside.
- Move inside the folder and (best to the following inside a screen) run `docker-compose up` this will start the docker instance.

Thus in short:
```
mkdir mywikibase
cd mywikibase
wget https://raw.githubusercontent.com/wmde/wikibase-docker/master/docker-compose.yml
docker-compose up
```

You should start to see the following;

```
Creating network "mywikibase_default" with the default driver
Creating volume "mywikibase_mediawiki-mysql-data" with default driver
Creating volume "mywikibase_mediawiki-images-data" with default driver
Creating volume "mywikibase_query-service-data" with default driver
Creating volume "mywikibase_quickstatements-data" with default driver
```

There you also see that mywikibase is the prefix for the docker instances which can be usefull when running multiple instances on a single machine.

Once its finished and you see the 10 second throtteling of blazegraph (waiting for edits) you should be able to access the website at <http://localhost:8181> or <http://[server.name]:8181>

**Please note that this is a very insecure wikibase with default username & password, this will be handled later but would require a reset...**

## When blazegraph is out of sync...
I did encounter sometimes that upon startup blazegraph was out of sync since the data is to old. By running the following command (assuming mywikibase and localhost):

`docker exec mywikibase_wdqs_1 ./runUpdate.sh -h http://wdqs.svc:9999 -- --wikibaseHost localhost:8181 --wikibaseScheme http --entityNamespaces 120,122 -s 20180301000000`

It forces the synchronization and once it is finished and it is throtteling you can stop it again to let wikibase take over (on a new wikibase this should only take seconds or less).

## LocalSettings.php
***Please note that you should copy the LocalSettings.php after the inital startup as when the file is present the installation of the wikibase will not happen and you get json parse errors like ">" invalid... ***

The local settings file contains additional settings for your wikibase. The best way to obtain it is to copy it from your running docker instance and keep that in your folder next to your yml file.

To copy it from your running docker instance we make use of the folder name where you have placed the yml file in. Make sure your are inside the folder (mywikibase) and execute the following:

```
docker cp mywikibase_wikibase_1:/var/www/html/LocalSettings.php ./LocalSettings.php
```

This will copy the local settings file from your wikibase into your folder.
Then you can edit it with your favorite editor...

### Interesting parts of LocalSettings.php

#### Changing the name of your wikibase
```
$wgSitename = "mywikibase";
```

*You should change the user name / password / schema but that will be discussed later...*

**Here are some of the things I have added to the file at the very end...**

#### Account creation is disabled and anonymous edits are also disabled
```
$wgGroupPermissions['*']['edit'] = true;
$wgGroupPermissions['*']['createaccount'] = false;
```

#### Wiki logo

Changing the default wikilogo shown on the website

```
$wgLogo = "$wgResourceBasePath/images/200px-Wikidata-logo.svg.png";
```

#### Formatter URL

Which property is used to reformat URL's 

```
$wgWBRepoSettings['formatterUrlProperty'] = 'P1';
```

#### Default skin

Changing the default theme / skin of the website

```
$wgDefaultSkin = "chameleon";
```
#### SheX entity schema

Enabling the shex definition page to validate entities according to a developed shex scheme.

```
wfLoadExtension( 'EntitySchema' );
```



<!-- ## Making the values of a property link to an external website -->
<!-- If you want to link a value (e.g. an external identifier) to its corresponding webpage outside your wikibase you can do this as followed. -->

## Startup script

I have created a startup script that makes sure that all my settings are in there when starting a fresh wikibase

```
if [ -z "${STY}" ]; then
    echo "This script can only be executed from a screen session"
else
    # Prepare the containers
    docker-compose up --no-start

    # Start the docker and detach from it to run the next lines
    docker-compose up --detach

    # Update the LocalSettings.php after 60 seconds, gives wikibase time to prepare the installation, might need a bigger value on a slower machine
    sleep 60

    # Start the update procedure
    # Setup logo
    docker cp 200px-Wikidata-logo.svg.png wikistrains_wikibase_1:/var/www/html/images/200px-Wikidata-logo.svg.png

    # Setup skin https://github.com/lingua-libre/llskin
    git clone https://github.com/thingles/foreground.git
    docker cp foreground wikistrains_wikibase_1:/var/www/html/skins/foreground

    # Setup another skin https://github.com/cmln/chameleon/releases/tag/2.1.0
    wget -nc https://github.com/cmln/chameleon/archive/2.1.0.tar.gz
    tar -xvf 2.1.0.tar.gz
    docker cp chameleon-2.1.0 wikistrains_wikibase_1:/var/www/html/skins/chameleon

    # Still need to prepare some docker php update commands for the skins

    # Setup local settings
    docker cp LocalSettings.php wikistrains_wikibase_1:/var/www/html/LocalSettings.php

    # Rebuild such that the formatter URL works
    docker exec wikistrains_wikibase_1 php extensions/Wikibase/repo/maintenance/rebuildPropertyInfo.php --rebuild-all --force

    echo "Finished startup script"
fi

```



#### Future topics

- Installation of a new skin (after a reboot)
- Backup your data and restore an instance
- Change hostname and rebuild the RDF database
- Bots?



<sup>Do you want to help to improve this [document](https://gitlab.com/jjkoehorst/koehorst/blob/master/content/post/WikiBase/index.md)? Make a pull request!</sup>

