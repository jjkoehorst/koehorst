---
title: iBioSystems
summary: Educate about the integrative wet-dry cycle to study genotype-phenotype relationships in prokaryotes and simple eukaryotes.
tags:
  - Teaching
date: "2018-09-11T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://ssc.wur.nl/Handbook/Course/SSB-20806

# image:
#   caption: Photo by Toa Heftiba on Unsplash
# focal_point: Smart
---

