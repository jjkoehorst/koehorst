---
title: LinkedData
summary: Linked Data technologies enable (research) data sharing in ways that are Findable, Accessible, Interoperable, and Reusable (FAIR).
tags:
  - Linked data
date: "2018-09-11T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://ssc.wur.nl/Handbook/Course/INF-33306

# image:
#   caption: Photo by Toa Heftiba on Unsplash
# focal_point: Smart
---
