---
title: Big Data Analysis in the Life Sciences
summary: Technological and scientific advances have pushed the Life sciences into the BIG data era.
tags:
  - UNLOCK
  - Infrastructure
  - Biotechnology
date: "2018-09-11T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://www.vlaggraduateschool.nl/en/courses/course/BDA19.htm

# image:
#   caption: Photo by Toa Heftiba on Unsplash
# focal_point: Smart
---
