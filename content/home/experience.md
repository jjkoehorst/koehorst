+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = false  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Researcher"
  company = "Wageningen University"
  company_url = "https://www.wur.nl"
  location = "Wageningen, the Netherlands"
  date_start = "2019-01-25"
  date_end = ""
  description = """Researcher and Teacher at Wageningen University
  Responsibilities include:
  
  * Bioinformatics
  * Infrastructure development
  * Teaching
  """

[[experience]]
  title = "PhD Candidate"
  company = "Wageningen University"
  company_url = "https://www.wur.nl"
  location = "Wageningen, the Netherlands"
  date_start = "2012-01-01"
  date_end = "2019-01-25"
  description = """A PhD in the field of genomics with a FAIR twist."""

[[experience]]
  title = "Msc Bioinformatics"
  company = "Wageningen University"
  company_url = "https://www.wur.nl"
  location = "Wageningen, the Netherlands"
  date_start = "2012-01-01"
  date_end = "2019-01-25"
  description = """A master in Bioinformatics."""

[[experience]]
  title = "Bsc Life Sciences"
  company = "Hogeschool Arnhem Nijmegen"
  company_url = "https://www.wur.nl"
  location = "Nijmegen, the Netherlands"
  date_start = "2012-01-01"
  date_end = "2019-01-25"
  description = """A bachelor in the field of...."""

+++
