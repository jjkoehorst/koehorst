---
title: "High throughput functional comparison of 432 genome sequences of pseudomonas using a semantic data framework"
date: 2016-01-01
publishDate: 2019-07-10T06:57:21.440737Z
authors: ["JJ Koehorst", "JCJ van Dam", "RGA van Heck", "E Saccenti", "VAP Martins dos Santos", "M Suarez-Diez", "PJ Schaap"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Sci Rep*"
---

