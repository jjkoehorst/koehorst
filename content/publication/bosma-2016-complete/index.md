---
title: "Complete genome sequence of thermophilic Bacillus smithii type strain DSM 4216 T"
date: 2016-01-01
publishDate: 2019-07-10T06:54:39.704419Z
authors: ["Elleke F Bosma", "Jasper J Koehorst", "Sacha AFT van Hijum", "Bernadet Renckens", "Bastienne Vriesendorp", "Antonius HP van de Weijer", "Peter J Schaap", "Willem M de Vos", "John van der Oost", "Richard van Kranenburg"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Standards in genomic sciences*"
---

