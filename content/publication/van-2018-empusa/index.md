---
title: "The Empusa code generator: bridging the gap between the intended and the actual content of RDF resources"
date: 2018-01-01
publishDate: 2019-07-10T06:57:21.441765Z
authors: ["Jesse CJ van Dam", "Jasper J Koehorst", "Peter J Schaap", "Maria Suarez-Diez"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*arXiv preprint arXiv:1812.04386*"
---

