---
title: "Interoperable genome annotation with GBOL, an extendable infrastructure for functional data mining"
date: 2017-01-01
publishDate: 2019-07-10T06:57:21.436087Z
authors: ["Jesse CJ van Dam", "Jasper Jan J Koehorst", "Jon Olav Vik", "Peter J Schaap", "Maria Suarez-Diez"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*bioRxiv*"
---

