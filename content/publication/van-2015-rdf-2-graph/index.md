---
title: "RDF2Graph a tool to recover, understand and validate the ontology of an RDF resource"
date: 2015-01-01
publishDate: 2019-07-10T06:54:39.700735Z
authors: ["Jesse CJ van Dam", "Jasper J Koehorst", "Peter J Schaap", "Vitor Ap Martins Dos Santos", "Maria Suarez-Diez"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Journal of biomedical semantics*"
---

