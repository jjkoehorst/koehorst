---
title: "SAPP: functional genome annotation and analysis through a semantic framework using FAIR principles"
date: 2017-01-01
publishDate: 2019-07-10T06:57:21.437935Z
authors: ["Jasper J Koehorst", "Jesse CJ van Dam", "Edoardo Saccenti", "Vitor AP Martins dos Santos", "Maria Suarez-Diez", "Peter J Schaap"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Bioinformatics*"
---

