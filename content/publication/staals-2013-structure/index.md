---
title: "Structure and activity of the RNA-targeting Type III-B CRISPR-Cas complex of Thermus thermophilus"
date: 2013-01-01
publishDate: 2019-07-10T06:54:39.693302Z
authors: ["Raymond HJ Staals", "Yoshihiro Agari", "Saori Maki-Yonekura", "Yifan Zhu", "David W Taylor", "Esther Van Duijn", "Arjan Barendregt", "Marnix Vlot", "Jasper J Koehorst", "Keiko Sakamoto", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Molecular cell*"
---

