---
title: "Complete genome sequence of Akkermansia glycaniphila strain PytT, a mucin-degrading specialist of the reticulated python gut"
date: 2017-01-01
publishDate: 2019-07-10T06:57:21.432380Z
authors: ["Janneke P Ouwerkerk", "Jasper J Koehorst", "Peter J Schaap", "Jarmo Ritari", "Lars Paulin", "Clara Belzer", "Willem M de Vos"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Genome Announc.*"
---

