---
title: "Comparative genomics of Streptococcus pyogenes M1 isolates differing in virulence and propensity to cause systemic infection in mice"
date: 2015-01-01
publishDate: 2019-07-10T06:54:39.698658Z
authors: ["Anne Fiebig", "Torsten G Loof", "Anshu Babbar", "Andreas Itzek", "Jasper J Koehorst", "Peter J Schaap", "D Patric Nitsche-Schmitz"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*International Journal of Medical Microbiology*"
---

