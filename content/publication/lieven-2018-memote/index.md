---
title: "Memote: A community-driven effort towards a standardized genome-scale metabolic model test suite"
date: 2018-01-01
publishDate: 2019-07-10T06:57:21.439826Z
authors: ["Christian Lieven", "Moritz Emanuel Beber", "Brett G Olivier", "Frank T Bergmann", "Meric Ataman", "Parizad Babaei", "Jennifer A Bartell", "Lars M Blank", "Siddharth Chauhan", "Kevin Correia", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*BioRxiv*"
---

