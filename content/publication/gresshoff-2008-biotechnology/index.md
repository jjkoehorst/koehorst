---
title: "A Biotechnology-Genetics-Genomics (BGG) platform for improvements of growth, adaptability and sustainable biofuel and bioenergy production of Pongamia Pinnata"
date: 2008-01-01
publishDate: 2019-07-10T06:57:43.070790Z
authors: ["Peter M Gresshoff", "Stephen Kazakoff", "Bandana Biswas", "Qunyi Jiang", "Michael Immelfort", "Jasper-Jan Koehorst", "Jessica Vogt", "Jessica Dalton-Morgan", "Alvin van Niekerk", "Paul Berkman", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: ""
---

