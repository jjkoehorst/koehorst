---
title: "Diversity of tryptophan halogenases in sponges of the genus Aplysina"
date: 2019-01-01
publishDate: 2019-07-10T06:57:43.075290Z
authors: ["Johanna Gutleben", "Jasper J Koehorst", "Kyle McPherson", "Shirley Pomponi", "René H Wijffels", "Hauke Smidt", "Detmer Sipkema"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*FEMS Microbiology Ecology*"
---

