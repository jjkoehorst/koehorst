---
title: "Interoperable genome annotation with GBOL, an extendable infrastructure for functional data mining"
date: 2017-01-01
publishDate: 2019-07-10T06:57:43.074003Z
authors: ["JJJ Koehorst", "JO Vik", "PJ Schaap", "M Suarez-Diez", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: ""
---

