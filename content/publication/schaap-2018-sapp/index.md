---
title: "SAPP: functional genome annotation and analysis through a semantic framework using FAIR principles"
date: 2018-01-01
publishDate: 2019-07-10T06:57:21.442828Z
authors: ["PJ Schaap", "JJ Koehorst", "JCJ van Dam", "E Saccenti", "VAP Martins dos Santos", "M Suarez-Diez"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*Scientific Symposium FAIR Data Sciences for Green Life Sciences*"
---

