---
title: "A genomic view on syntrophic versus non-syntrophic lifestyle in anaerobic fatty acid degrading communities"
date: 2014-01-01
publishDate: 2019-07-10T06:54:39.702004Z
authors: ["Petra Worm", "Jasper J Koehorst", "Michael Visser", "Vicente T Sedano-Núñez", "Peter J Schaap", "Caroline M Plugge", "Diana Z Sousa", "Alfons JM Stams"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Biochimica et Biophysica Acta (BBA)-Bioenergetics*"
---

