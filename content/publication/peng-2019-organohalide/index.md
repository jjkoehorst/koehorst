---
title: "Organohalide-respiring Desulfoluna species isolated from marine environments"
date: 2019-01-01
publishDate: 2019-07-10T06:57:43.072138Z
authors: ["Peng Peng", "Tobias Goris", "Yue Lu", "Bart Nijsse", "Anna Burrichter", "David Schleheck", "Jasper J Koehorst", "Jie Liu", "Detmer Sipkema", "Jaap S Sinninghe Damste", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*bioRxiv*"
---

