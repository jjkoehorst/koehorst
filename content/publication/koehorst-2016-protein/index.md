---
title: "Protein domain architectures provide a fast, efficient and scalable alternative to sequence-based methods for comparative functional genomics"
date: 2016-01-01
publishDate: 2019-07-10T06:54:39.703041Z
authors: ["Jasper J Koehorst", "Edoardo Saccenti", "Peter J Schaap", "Vitor AP Martins dos Santos", "Maria Suarez-Diez"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*F1000Research*"
---

