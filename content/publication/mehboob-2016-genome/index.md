---
title: "Genome and proteome analysis of P seudomonas chloritidismutans AW-1 T that grows on n-decane with chlorate or oxygen as electron acceptor"
date: 2016-01-01
publishDate: 2019-07-10T06:54:39.696889Z
authors: ["Farrakh Mehboob", "Margreet J Oosterkamp", "Jasper J Koehorst", "Sumaira Farrakh", "Teun Veuskens", "Caroline M Plugge", "Sjef Boeren", "Willem M de Vos", "Gosse Schraa", "Alfons JM Stams", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Environmental microbiology*"
---

