---
title: "Comparison of 432 Pseudomonas strains through integration of genomic, functional, metabolic and expression data"
date: 2016-01-01
publishDate: 2019-07-10T06:57:21.430554Z
authors: ["Jasper J Koehorst", "Jesse CJ Van Dam", "Ruben GA Van Heck", "Edoardo Saccenti", "Vitor AP Martins Dos Santos", "Maria Suarez-Diez", "Peter J Schaap"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Scientific reports*"
---

