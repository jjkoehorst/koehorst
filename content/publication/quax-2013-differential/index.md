---
title: "Differential translation tunes uneven production of operon-encoded proteins"
date: 2013-01-01
publishDate: 2019-07-10T06:54:39.695155Z
authors: ["Tessa EF Quax", "Yuri I Wolf", "Jasper J Koehorst", "Omri Wurtzel", "Richard van der Oost", "Wenqi Ran", "Fabian Blombach", "Kira S Makarova", "Stan JJ Brouns", "Anthony C Forster", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Cell reports*"
---

