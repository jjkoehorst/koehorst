---
title: "Assessing the metabolic diversity of streptococcus from a protein domain point of view"
date: 2015-01-01
publishDate: 2019-07-10T06:54:39.699604Z
authors: ["Edoardo Saccenti", "David Nieuwenhuijse", "Jasper J Koehorst", "Vitor AP Martins dos Santos", "Peter J Schaap"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*PloS one*"
---

