---
title: "Genome analyses of the carboxydotrophic sulfate-reducers Desulfotomaculum nigrificans and Desulfotomaculum carboxydivorans and reclassification of Desulfotomaculum caboxydivorans as a later synonym of Desulfotomaculum nigrificans"
date: 2014-01-01
publishDate: 2019-07-10T06:54:39.694210Z
authors: ["Michael Visser", "Sofiya N Parshina", "Joana I Alves", "Diana Z Sousa", "Inês AC Pereira", "Gerard Muyzer", "Jan Kuever", "Alexander V Lebedinsky", "Jasper J Koehorst", "Petra Worm", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Standards in genomic sciences*"
---

