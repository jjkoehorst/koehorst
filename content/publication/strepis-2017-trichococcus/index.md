---
title: "Trichococcus species as catalysts for biotechnological production of 1, 3-propanediol"
date: 2017-01-01
publishDate: 2019-07-10T06:57:21.436926Z
authors: ["Nikolaos Strepis", "Jasper Koehorst", "Henry Benavides Naranjo", "Peter J Schaap", "Alfons Johannes Maria Stams", "Diana Zita Machado Sousa"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*Microbiology Centennial Symposium 2017 ‘Exploring Microbes for the Quality of Life’: book of abstracts*"
---

