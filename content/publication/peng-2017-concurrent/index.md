---
title: "Concurrent haloalkanoate degradation and chlorate reduction by Pseudomonas chloritidismutans AW-1T"
date: 2017-01-01
publishDate: 2019-07-10T06:57:21.435135Z
authors: ["Peng Peng", "Ying Zheng", "Jasper J Koehorst", "Peter J Schaap", "Alfons JM Stams", "Hauke Smidt", "Siavash Atashgahi"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Appl. Environ. Microbiol.*"
---

