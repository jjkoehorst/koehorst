---
title: "Persistence of functional protein domains in mycoplasma species and their role in host specificity and synthetic minimal life"
date: 2017-01-01
publishDate: 2019-07-10T06:57:21.434171Z
authors: ["Tjerko Kamminga", "Jasper J Koehorst", "Paul Vermeij", "Simen-Jan Slagman", "Vitor AP Martins dos Santos", "Jetta JE Bijlsma", "Peter J Schaap"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Frontiers in cellular and infection microbiology*"
---

