---
title: "Comparative genomics highlights symbiotic capacities and high metabolic flexibility of the marine genus Pseudovibrio"
date: 2018-01-01
publishDate: 2019-07-10T06:57:21.438877Z
authors: ["Dennis Versluis", "Bart Nijsse", "Mohd Azrul Naim", "Jasper J Koehorst", "Jutta Wiese", "Johannes F Imhoff", "Peter J Schaap", "Mark WJ van Passel", "Hauke Smidt", "Detmer Sipkema"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Genome biology and evolution*"
---

