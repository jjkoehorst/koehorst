---
title: "Reverse methanogenesis and respiration in methanotrophic archaea"
date: 2017-01-01
publishDate: 2019-07-10T06:57:21.433294Z
authors: ["Peer HA Timmers", "Cornelia U Welte", "Jasper J Koehorst", "Caroline M Plugge", "Mike SM Jetten", "Alfons JM Stams"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Archaea*"
---

