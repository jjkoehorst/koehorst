---
title: "Expected and observed genotype complexity in prokaryotes: correlation between 16S-rRNA phylogeny and protein domain content"
date: 2018-01-01
publishDate: 2019-07-10T06:57:21.443746Z
authors: ["Jasper J Koehorst", "Edoardo Saccenti", "Vitor Martins dos Santos", "Maria Suarez-Diez", "Peter J Schaap"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*bioRxiv*"
---

