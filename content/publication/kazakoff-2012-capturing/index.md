---
title: "Capturing the biofuel wellhead and powerhouse: the chloroplast and mitochondrial genomes of the leguminous feedstock tree Pongamia pinnata"
date: 2012-01-01
publishDate: 2019-07-10T06:54:39.692455Z
authors: ["Stephen H Kazakoff", "Michael Imelfort", "David Edwards", "Jasper Koehorst", "Bandana Biswas", "Jacqueline Batley", "Paul T Scott", "Peter M Gresshoff"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*PLoS One*"
---

