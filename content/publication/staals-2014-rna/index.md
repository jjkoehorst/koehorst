---
title: "RNA targeting by the type III-A CRISPR-Cas Csm complex of Thermus thermophilus"
date: 2014-01-01
publishDate: 2019-07-10T06:54:39.696045Z
authors: ["Raymond HJ Staals", "Yifan Zhu", "David W Taylor", "Jack E Kornfeld", "Kundan Sharma", "Arjan Barendregt", "Jasper J Koehorst", "Marnix Vlot", "Nirajan Neupane", "Koen Varossieau", " others"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Molecular cell*"
---

