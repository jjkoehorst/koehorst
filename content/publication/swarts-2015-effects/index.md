---
title: "Effects of Argonaute on gene expression in Thermus thermophilus"
date: 2015-01-01
publishDate: 2019-07-10T06:54:39.697875Z
authors: ["Daan C Swarts", "Jasper J Koehorst", "Edze R Westra", "Peter J Schaap", "John van der Oost"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*PloS one*"
---

