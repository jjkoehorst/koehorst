---
title: "The Constructor: a web application optimizing cloning strategies based on modules from the registry of standard biological parts"
date: 2012-01-01
publishDate: 2019-07-10T06:54:39.691238Z
authors: ["Matthijn C Hesselman", "Jasper J Koehorst", "Thijs Slijkhuis", "Dorett I Odoni", "Floor Hugenholtz", "MarkW J van Passel"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Journal of biological engineering*"
---

